using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.Storage;
using Microsoft.Azure.Storage.Blob;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using miniwms.Data;
using miniwms.Models;
using Microsoft.Extensions.Configuration;
using System.Configuration;

// namespace - helps organize projects, helps control scopes
namespace miniwms.Controllers
{
    // public class - no restrictions on access CargoController - name
    // Controller - class we're stating from the MVC
    // C# has classical inheritance. that's the  " : " between CargoController & Controller.
    // Controllers do a whole bunch of stuff and we are inheriting all that functionality.
    public class CargoController : Controller
    {
        // private readonly he benefit is that you can't inadvertently change it from
        //another part of that class after it is initialized.
        //Possible to get an example on the above???
        private readonly ApplicationDbContext _context;

        private readonly ILogger<CargoController> _logger;

        private readonly IConfiguration _configuration;


        private CloudBlobContainer container;

        //CargoController(ApplicationDbContext)
        // a little confusing syntax wise but assuming it's
        // reassigning ApplicationDbContext into _context
        // Dependency Injection - Specific to dot net MVC - any parameters put into the constructor are depedencies.
        // It's saying "hey I need to use this"
        public CargoController(
            ApplicationDbContext context,
            ILogger<CargoController> logger,
            IConfiguration configuration
        )
        {
            _context = context;
            _logger = logger;
            _configuration = configuration;
        }

        // GET: Cargo
        public async Task<IActionResult>
        Index() // when someone makes a request to our server, an <IActionResult> is returned.
        {
            // Get request when going to Cargo
            // return the View, but in the parameters of the view function
            // we're awaiting the DB query to execute
            _logger.LogWarning("testing 1 2 3", null);

            return View(await _context
                .Cargo // go to Db.Cargo
                .Include(Cargo => (Cargo.Shipment)) //Include Cargo.Shipment (ID)
                .ToListAsync()); // generates list
        }

        // GET: Cargo/Details/5
        public async Task<IActionResult>
        Details(int? id) // Details takes an integer (id) so we can look up the specific cargo by that number // public async Task immediately tells this method will be asynchronous
        {
            if (
                id == null // if the number is null or not found - return a view of NotFound()
            )
            {
                return NotFound();
            }

            // set our variable of cargo -> then query the DB and see if can find via the id number
            Cargo cargo =
                await _context
                    .Cargo
                    .Include(Cargo => (Cargo.Urls))
                    .FirstOrDefaultAsync(m => m.Id == id);
            if (cargo == null)
            {
                return NotFound(); // if not found, same as above
            }

            return View(cargo); // otherwise return the view with our cargo parameter
        }




        // GET: Cargo/Create
        public async Task<IActionResult>
        Create() // public async tells us this will be async - and we're in the Create controller now
        {
            var shipmentList = await _context.Shipment.ToListAsync(); // querying db for our shipment list
            ViewBag.shipmentList = shipmentList;

            var customerList = await _context.Customer.ToListAsync(); // querying db for customer list
            ViewBag.customerList = customerList;

            return View(); // returning the view of Create but now we have access to data to use in the HTML (i.e. delegating cargo to a certain shipment)
        }

        // POST: Cargo/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost] // methods of posting client data or form data to the server.
        [ValidateAntiForgeryToken] //  to prevent cross-site request forgery attacks
        public async Task<IActionResult>
        Create(
            [
                Bind(
                    "Id,TypeOfPallet,ProNumber,Length,Width,Height,Weight,Damage,Shipment,Customer,PieceCount,Destination,ArrivingFrom,LotNumber,Consignee,Marks")
            ]
            Cargo cargo
        ) // Bindings .... ?
        {
            if (
                ModelState.IsValid // ModelState.IsValid indicates if it was possible to bind the incoming values
            )
            // from the request to the model correctly and whether any explicitly specified
            // validation rules were broken during the model binding process.
            {
                var shipment =
                    await _context.Shipment.FindAsync(cargo.Shipment.Id);

                // modify cargo shipment to be this new shipment that we looked up in the DB
                cargo.Shipment = shipment;

                // await _context.SaveChangesAsync(); // wait for our database to save the above changes
                var customer =
                    await _context.Customer.FindAsync(cargo.Customer.Id);
                cargo.Customer = customer;
                _context.Add (cargo);
                await _context.SaveChangesAsync();

                return RedirectToAction(nameof(Index)); // returns us to the Index page of cargo when done
            }
            return View(cargo); // sets up the view for entering data
        }

        [HttpPost] // post method
        [AllowAnonymous]
        // this will allow anyone to ping the server (good for postman)
        public async Task<IActionResult>
        CreateByAPI(
            [FromBody] int Id,
            PalletType TypeOfPallet,
            double Length,
            double Width,
            double Height,
            double Weight,
            bool Damage,
            Shipment Shipment,
            Customer Customer
        ) // The above model is what we should be expecting in raw JSON format
        {
            if (
                ModelState.IsValid // If the model is good we can proceed to the below code
            )
            {
                var shipment = await _context.Shipment.FindAsync(Shipment.Id); // set shipment = to ShipmentId in the DB

                // modify cargo shipment to be this new shipment that we looked up in the DB
                // Shipment = shipment;
                //_context.Add (cargo);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return Ok("Succeeded"); // send back Succeeded as a response
        }

        // GET: Cargo/Edit/5
        public async Task<IActionResult>
        Edit(int? id) // very similar to above already explained!
        {
            var shipmentList = await _context.Shipment.ToListAsync(); // querying db for our shipment list
            ViewBag.shipmentList = shipmentList;

            var customerList = await _context.Customer.ToListAsync(); // querying db for customer list
            ViewBag.customerList = customerList;

            if (id == null)
            {
                return NotFound();
            }

            var cargo = await _context.Cargo.FindAsync(id);
            if (cargo == null)
            {
                return NotFound();
            }
            return View(cargo);
        }

        // POST: Cargo/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        // whenever someone is sending data to our server - we want to make sure they're coming from 'our website'
        // dot net core is constantly doing a crypto exchange with the browser - we just have to remember to validate the antiforgerytoken
        // any time we're taking info from the browser.
        [ValidateAntiForgeryToken]
        public async Task<IActionResult>
        Edit(
            int id,
            [Bind("Id,TypeOfPallet,Length,Width,Height,Weight,Damage,Shipment")]
            Cargo cargo
        )
        {
            if (id != cargo.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                // if the Model is good - update the DB with cargo
                // Are update & SaveChanges not kind of the same though? Why both?
                {
                    _context.Update (cargo);
                    await _context.SaveChangesAsync();
                }
                catch (
                    DbUpdateConcurrencyException // read up on this, still not 100%
                )
                // sounds similar to JS catch method
                {
                    if (!CargoExists(cargo.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(cargo);
        }
 
        // POST - Post Photo
        [HttpPost] // post method
        // this will allow anyone to ping the server (good for postman)
        public async Task<IActionResult>
        PostPicture([FromForm] int id, [FromForm] IFormFile picturefile) 
        // The above model is what we should be expecting in raw JSON format
        {
            // sett connection string to read from configuration (if possible) 
            // for now it's hard coded (not recommended)
        

            if (picturefile == null) {
                return Redirect($"~/Cargo/Details/{id}");
            }
            // var connectionString = _configuration.GetConnectionString("BlobStorage");

            var connectionString = _configuration.GetConnectionString("BlobStorage");


            // Logging in to CloudStorageAccount using the connectionString
            CloudStorageAccount storageAccount =
                CloudStorageAccount.Parse(connectionString);

            // Get a tool that will allow me to start modifying things for my 
            // storage account
            var blobClient = storageAccount.CreateCloudBlobClient();

            // specify which container we want to work with, in this case freightreport 
            container = blobClient.GetContainerReference("freightreport");

            // Normally the above 4 lines of code we be in a service to keep things simple. 

            // The below function/methods could be helpers to above that we would separate


            // Guid makes a unique identifying string/number 
            // set to g
            Guid g = Guid.NewGuid();


            // Reserving the space for what we're going to upload 
            // We're constructing a unique url / filename 
            CloudBlockBlob blockBlob =
                container
                    .GetBlockBlobReference($"picture/{g}/{picturefile.FileName}");
            blockBlob.Properties.ContentType = picturefile.ContentType;
            // the above line is specifying the filetype (png for example)


            // using statements are for memory management
            // when we have a heavy variable that we need to disposte of to clean up memory 
            // this "automatically" will close it and free space 
            using (var uploadFileStream = picturefile.OpenReadStream())
            {
                // upload our picture to our newly reservced Blob space 
                await blockBlob.UploadFromStreamAsync(uploadFileStream);
                
                // close the stream. 
                uploadFileStream.Close();
            }
        


            // get the result of wherever it was uploaded to. 
            var blobDestination = blockBlob.StorageUri;

            // End blob storage methods. 


            // enter database methods (adding to our DB) 

            // find the cargo via the id 
            var cargo = await _context.Cargo.FindAsync(id);

            // if no lists are present, we're going to make a new empty List of UrlPath
            if (cargo.Urls == null)
            {
                cargo.Urls = new List<UrlPath>();
            }
            // Define our UrlPath list with Id and Url 
            cargo
                .Urls
                .Add(new UrlPath {
                    Id = 0,
                    // find the PriimaryUri and convert it to a string for db 
                    Url = blobDestination.PrimaryUri.ToString()
                });
            // Finally save our changes in our database. 
            await _context.SaveChangesAsync();
            
            // redirect to the Index of Cargo view
             // *** ORIGINAL CODE BELOW ****
            //return RedirectToAction(nameof(Index));

              return RedirectToAction(nameof(Details), new { id = cargo.Id });
            // return Redirect($"~/Cargo/Details/{id}");
        }

        // GET - Photo Retrieval
        // this will allow anyone to ping the server (good for postman)
        public async Task<IActionResult> GetPicture(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cargo =
                await _context.Cargo.FirstOrDefaultAsync(m => m.Id == id);
            if (cargo == null)
            {
                return NotFound();
            }

            return View(cargo);
        }

        // GET: Cargo/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cargo =
                await _context.Cargo.FirstOrDefaultAsync(m => m.Id == id);
            if (cargo == null)
            {
                return NotFound();
            }

            return View(cargo); // same as above - returns a more detailed view of the piece of your cargo before you actually delete it
        }

        // POST: Cargo/Delete/5
        [HttpPost, ActionName("Delete")] // Action Name ("Delete") - another method provided.
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var cargo = await _context.Cargo.FindAsync(id);
            _context.Cargo.Remove (cargo);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        // Acess Modifier - private
        // bool - type (usually the second part that's in tiel in other functions, when they're in tiel they're a custom datatype that someone invented
        // the reason bool is blue is because it's native to c#)
        // CargoExists - name - a helper function - it's the only function that doesn't return an <IActionResult>
        // The only functions that can call this function are other functions in this file.
        private bool CargoExists(int id)
        {
            return _context.Cargo.Any(e => e.Id == id);
        }
    }
}
