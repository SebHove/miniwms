using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using miniwms.Data;
using miniwms.Models;

namespace miniwms.Controllers
{
    // You can write Attributes here that count for all methods below
    // but can also individually edit methods with their own attributes (for example check authorization / cookies)
    [Route("api/APICargoSend")]
    [ApiController]
    [AllowAnonymous]
    public class APICargoSend : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public APICargoSend(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/APICargoSend
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Cargo>>> GetCargo()
        {
            return await _context
                .Cargo
                .Include(Cargo => (Cargo.Shipment))
                .ToListAsync();
        }

        // GET: api/APICargoSend/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Cargo>> GetCargo(int id)
        {
            var cargo = await _context.Cargo.FindAsync(id);

            // crashes when trying to join cargo.Shipment.
            // var cargo = await _context.Cargo.Include(cargo => cargo.Shipment)
            // .FirstOrDefaultAsync(i => i.Id == id);
            if (cargo == null)
            {
                return NotFound();
            }

            return cargo;
        }

        // PUT: api/APICargoSend/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCargo(int id, Cargo cargo)
        {
            if (id != cargo.Id)
            {
                return BadRequest();
            }

            _context.Entry(cargo).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CargoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/APICargoSend
        [HttpPost("CargoPost")] // post method
        [AllowAnonymous]
        public async Task<IActionResult> CargoPost(Cargo cargo)
        {
            if (ModelState.IsValid)
            {
                var shipment =
                    await _context.Shipment.FindAsync(cargo.Shipment);
                cargo.Shipment = shipment;
                var customer =
                    await _context.Customer.FindAsync(cargo.Customer);
                cargo.Customer = customer;
                _context.Cargo.Add (cargo);
                await _context.SaveChangesAsync();
                return CreatedAtAction("GetCargo",
                new { id = cargo.Id },
                cargo);
            }
            return Ok("Succeeded");
        }

        [HttpPost("CargoSend")]
        [AllowAnonymous]
        public async Task<IActionResult>
        CargoSend(CargoSpectreResponse cargoSpectreResponse)
        {
            //Console.WriteLine(cargoSpectreResponse.Responses.Dimension.Code);
            var myCargo =
                new Cargo {
                    Customer = null,
                    Length =
                        cargoSpectreResponse
                            .Responses?
                            .Dimension?
                            .Info?
                            .Dimensions?
                            .Length
                            ?? 0.0f,
                    Width =
                        cargoSpectreResponse
                            .Responses
                            .Dimension
                            .Info
                            .Dimensions
                            .Width,
                    Height =
                        cargoSpectreResponse
                            .Responses
                            .Dimension
                            .Info
                            .Dimensions
                            .Height,
                    Weight =
                        cargoSpectreResponse
                            .Responses
                            .Dimension
                            .Info
                            .Dimensions
                            .Weight
                            .Gross,
                    ProNumber =
                        cargoSpectreResponse
                            .Responses
                            .Dimension
                            .Info
                            .CustomFields?
                            .ProNumber,
                    PieceCount =
                        cargoSpectreResponse
                            .Responses
                            .Dimension
                            .Info
                            .CustomFields?
                            .PieceCount
                            ?? 0,
                    Destination =
                        cargoSpectreResponse
                            .Responses
                            .Dimension
                            .Info
                            .CustomFields?
                            .Destination,
                    ArrivingFrom =
                        cargoSpectreResponse
                            .Responses
                            .Dimension
                            .Info
                            .CustomFields?
                            .ArrivingFrom,
                    LotNumber =
                        cargoSpectreResponse
                            .Responses
                            .Dimension
                            .Info
                            .CustomFields?
                            .LotNumber
                            ?? 0,
                    Consignee =
                        cargoSpectreResponse
                            .Responses
                            .Dimension
                            .Info
                            .CustomFields?
                            .Consignee,
                    Marks =
                        cargoSpectreResponse
                            .Responses
                            .Dimension
                            .Info
                            .CustomFields?
                            .Marks,
                };

            // foreach (var url in myCargo.Url) {
            //     using (var client = new WebClient())
            //     {
            //         var splitStringList = url.Split("\\").Length - 1;
            //         Uri uri = new Uri(url);
            //         client.DownloadFileAsync(uri, url.Split("\\")[splitStringList]);
            //         client.OpenReadAsync(uri, url);
            //         BlobContainerClient container = new BlobContainerClient(, freightReport)
            //     }
            // }
            if (myCargo.Urls == null){
                myCargo.Urls = new List<UrlPath>();
            }

            foreach (var url in cargoSpectreResponse.Responses.Snapshot.Directory.Images.Path) {

                  var fullUrl = "https://cargospectre.blob.core.windows.net/scans/" + url.Replace(@"\", @"/");

            myCargo.Urls.Add(new UrlPath {
                        Id = 0,
                        Url = fullUrl
                        });
            }

            var shipment = await _context.Shipment.FindAsync(2);

            myCargo.Shipment = shipment;

            var customer = await _context.Customer.FindAsync(1);
            myCargo.Customer = customer;

            _context.Cargo.Add (myCargo);
            await _context.SaveChangesAsync();
            if (myCargo.Shipment != null)
            {
                myCargo.Shipment.Cargo = null;
            }
            if (myCargo.Customer != null)
            {
                myCargo.Customer.Cargo = null;
            }
            return Ok(myCargo);
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Respond(Cargo cargo)
        {
            var shipment = await _context.Shipment.FindAsync(cargo.Shipment.Id);
            return Ok("This post works!");
        }

        // DELETE: api/APICargoSend/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCargo(int id)
        {
            var cargo = await _context.Cargo.FindAsync(id);
            if (cargo == null)
            {
                return NotFound();
            }

            _context.Cargo.Remove (cargo);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool CargoExists(int id)
        {
            return _context.Cargo.Any(e => e.Id == id);
        }
    }
}
