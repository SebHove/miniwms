using System;

namespace miniwms.Models
{
    public class CargoSpectreResponse
    {
        public Responses Responses { get; set; }
    }

    public class Responses
    {
        public Dimension Dimension { get; set; }

        public Snapshot Snapshot { get; set; }

        // public Ping Ping { get; set; }
    }

    public class Dimension
    {
        public int Code { get; set; }

        public Info Info { get; set; }
    }

    public class Snapshot
    {
        public int Code { get; set; }

        public Directory Directory { get; set; }
    }

    public class Directory
    {
        public Clouds Clouds { get; set; }

        public Images Images { get; set; }

        public Thumbnails Thumbnails { get; set; }

        public Misc Misc { get; set; }
    }

    public class Clouds
    {
        public string[] Path { get; set; }
    }

    public class Images
    {
        public string[] Path { get; set; }
    }

    public class Thumbnails
    {
        public string[] Path { get; set; }
    }

    public class Misc
    {
        public string[] Path { get; set; }
    }

    public class Info
    {
        public int CargoId { get; set; }

        public string Name { get; set; }

        public string Barcode { get; set; }

        public string Comments { get; set; }

        public DateTime ScanDate { get; set; }

        public string MachineName { get; set; }

        public string ScanGuid { get; set; }

        public Dimensions Dimensions { get; set; }

        public Units Units { get; set; }

        public CustomFields CustomFields { get; set; }
    }

    public class Dimensions
    {
        public float Length { get; set; }

        public float Width { get; set; }

        public float Height { get; set; }

        public float Volume { get; set; }

        public Weight Weight { get; set; }
    }

    public class Weight
    {
        public float Net { get; set; }

        public float Gross { get; set; }

        public float Tare { get; set; }
    }

    public class Units
    {
        public string Length { get; set; }

        public string Volume { get; set; }

        public string Weight { get; set; }
    }

    public class CustomFields
    {
        public string ProNumber { get; set; }

        public int PieceCount { get; set; }

        public string Destination { get; set; }

        public string ArrivingFrom { get; set; }

        public int LotNumber { get; set; }

        public string Consignee { get; set; }

        public string Marks { get; set; }
    }
}
