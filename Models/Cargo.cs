using System.ComponentModel;
using System.Collections.Generic;
// import statement - using code that is reference anywhere else - you'll have to import this. (good practice to alphabetize and get rid of greyed out import statements)
// can't have duplicates.
namespace miniwms.Models
// can be anything you want - organizing your folder structure.
// usually it will map to the same structure as your actual folder structure.
// every C# file should have a name space for that file
{
    public class Cargo
    // maps to the name of the file which is Cargo.cs
    // public means that things outside this file are allowed to reference it
    // private means outside this file are not allowed to reference it
    // protected means in this file and any children of this this file (even if they're outside of this file) i.e. if there was a subclass
    // of cargo. That would have access to the protected variables / entities.
    // public private protected are access modifiers - they state who is allowed to access this variable.
    // class is usually the datatype. In this case class is a built in c# datatype.
    // a constructor is a function that creates an instance of this class (we didn't make this explicity, but there is one by default)
    // name "Cargo" - in tiel - should be something that makes sense to you.
    {
        // these are all property - all of these line items are properties.
        // they have a similar structure. The first part is the access modifier (works same way as above), things outside of this class of this class are
        // able to access it.
        // lean towards making things public.
        // public int iD
        // public - Access Modifier (public / private / protected - are your options for setting. If you forget it, it sets private by default.)
        // int = datatype (telling c# what kind of data you expect to be here, could be a built in datatype, or be one that we made up (such as PalletType) or from an out
        // side library as well.)
        // variable name - if it's public you capitalize the first letter, if it's private, you use lowercase on the first letter (camelcase);
        // { get; set; } - being explicit because of entity framework - allows you to get & set values. (side note on side effects)
        // if it's not a database object - it's not that important.
        // [DisplayName("Type of Pallet")] - attribute, also known as a decorator.
        // It gives a little bit of extra information that we wouldn't have gotten before.
        // you don't have to specify the data type with an equals sign - c# will figure out hte datatype for you and do it at compile time.
        //          for example var testVar = some function.   or var someNumber = 5     <-- c# can tell it's an integer.
        // ***Always be explicit about data types for practice***
        // Id - name
        public int Id { get; set; }

        // Display name will now display all over the browser.
        [DisplayName("Type of Pallet")]
        public PalletType TypeOfPallet { get; set; }

        public string ProNumber { get; set; }

        public double Length { get; set; }

        public double Width { get; set; }

        public double Height { get; set; }

        public double Weight { get; set; }

        public bool Damage { get; set; }

        public int? ShipmentId { get; set; }

        public virtual Shipment Shipment { get; set; }

        public int? CustomerId { get; set; }

        public virtual Customer Customer { get; set; }

        [DisplayName("Piece Count")]
        public int PieceCount { get; set; }

        // Destination would probably need some FK
        // in our WH system
        // With CMS you would destine something to LAX
        // and once labeled you could create a load
        // plan with all filtered pieces heading to LAX
        public string Destination { get; set; }

        [DisplayName("Arriving From")]
        public string ArrivingFrom { get; set; }

        // LotNumber i.e. 102165-12
        // Where 102165 is the Shipment Number
        // 12 is this specifics lot number
        public int LotNumber { get; set; }

        public string Consignee { get; set; }

        public string Marks { get; set; }


        public List<UrlPath> Urls { get; set; }
       
    }

    public class UrlPath {
        public int Id {get; set;}
        public string Url {get; set;}
    }
    public enum PalletType
    {
        HeatTreated,
        Wooden,
        Metal,
        Tote,
        Crate,
        SkeletonCrate,
        NonHeatTreated,
        EuropeanPallet
    }
}
