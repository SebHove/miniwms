using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text.Json.Serialization;

namespace miniwms.Models
{
    public class Shipment
    {
        public int Id { get; set; }

        [DisplayName("Container Number")]
        public string ContainerNumber { get; set; }

        [DisplayName("Port of Load")]
        public string PortOfLoad { get; set; }

        [DisplayName("Port of Discharge")]
        public string PortOfDischarge { get; set; }

        [DisplayName("Voyage Number")]
        public string VoyageNumber { get; set; }

        [DisplayName("Shipment Number")]
        public int ContainerSize { get; set; }

        [DisplayName("Shipment Number")]
        public string ShipmentNumber { get; set; }

        [DisplayName("Number Of Items")]
        public float NumberOfItems { get; set; }

        [DisplayName("Total Weight")]
        public double TotalWeight { get; set; }

        [DisplayName("Total Volume")]
        public double TotalVolume { get; set; }

        [DisplayName("Seal Number")]
        public string SealNumber { get; set; }

        [DisplayName("Date Received")]
        public DateTime ShipmentReceived { get; set; }

        // Foreign Key to CargoId
        public ICollection<Cargo> Cargo { get; set; }
    }
}
