using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace miniwms.Models
{
    public class Customer
    {
      public int Id {get; set;}
      public string CustomerName {get; set;}
      public string Country {get; set;}
      public string Email {get; set;}
      public string Phone {get; set;}
      public string Address {get; set;}
      public bool LicenseActive {get; set;}

      // Foreign Key to Cargo
     
      public ICollection<Cargo> Cargo {get; set;}

    }

}