namespace miniwms.Models
{
    public class CargoRequest
    {
        public int Id { get; set; }

        public PalletType TypeOfPallet { get; set; }

        public int CargoNumber { get; set; }

        public double Width { get; set; }

        public double Length { get; set; }

        public float Height { get; set; }

        public string Name { get; set; }

        public bool Damage { get; set; }
    }
}
