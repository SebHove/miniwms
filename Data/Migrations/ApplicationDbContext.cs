using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using miniwms.Models;

namespace miniwms.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public DbSet<Cargo> Cargo {get; set;}
        
        public DbSet<Customer> Customer {get; set;}

        public DbSet<Shipment> Shipment {get; set;}
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
    }
}
