﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace miniwms.Migrations
{
    public partial class CargoShipmentUpdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ContainerSize",
                table: "Shipment",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "PortOfDischarge",
                table: "Shipment",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PortOfLoad",
                table: "Shipment",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SealNumber",
                table: "Shipment",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "VoyageNumber",
                table: "Shipment",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ProNumber",
                table: "Cargo",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ContainerSize",
                table: "Shipment");

            migrationBuilder.DropColumn(
                name: "PortOfDischarge",
                table: "Shipment");

            migrationBuilder.DropColumn(
                name: "PortOfLoad",
                table: "Shipment");

            migrationBuilder.DropColumn(
                name: "SealNumber",
                table: "Shipment");

            migrationBuilder.DropColumn(
                name: "VoyageNumber",
                table: "Shipment");

            migrationBuilder.DropColumn(
                name: "ProNumber",
                table: "Cargo");
        }
    }
}
