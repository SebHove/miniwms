﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace miniwms.Migrations
{
    public partial class CargoShipmentUpdate2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ArrivingFrom",
                table: "Cargo",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Consignee",
                table: "Cargo",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Destination",
                table: "Cargo",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "LotNumber",
                table: "Cargo",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Marks",
                table: "Cargo",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PieceCount",
                table: "Cargo",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ArrivingFrom",
                table: "Cargo");

            migrationBuilder.DropColumn(
                name: "Consignee",
                table: "Cargo");

            migrationBuilder.DropColumn(
                name: "Destination",
                table: "Cargo");

            migrationBuilder.DropColumn(
                name: "LotNumber",
                table: "Cargo");

            migrationBuilder.DropColumn(
                name: "Marks",
                table: "Cargo");

            migrationBuilder.DropColumn(
                name: "PieceCount",
                table: "Cargo");
        }
    }
}
